# Raytracer
Raytracer made using doc. from: "Raytracer in a weekend"


Note that there are 2 .exe's available to render the image yourself:
1. Open CMD
2. Type: "InsertFilePath"/SoftwareRaytracer.exe > image.ppm
  - If no text appears on the CMD you have misstyped!
3. Wait, for the large scene this might take a considerable amount of time!
  - Text will appear on the CMD when finished.
4. Find the image.ppm file: C:/Users/"Username"/image.ppm
4. Open the .ppm using an online .ppm viewer, I personally used: http://www.cs.rhodes.edu/welshc/COMP141_F16/ppmReader.html


Doc:
Raytracer in a weekend: https://raytracing.github.io/books/RayTracingInOneWeekend.html
